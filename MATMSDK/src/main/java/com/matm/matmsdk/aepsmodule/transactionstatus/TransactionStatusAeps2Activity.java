package com.matm.matmsdk.aepsmodule.transactionstatus;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.model.Progress;
import com.matm.matmsdk.aepsmodule.utils.AepsSdkConstants;
import com.matm.matmsdk.aepsmodule.utils.Constants;
import com.matm.matmsdk.aepsmodule.utils.Session;
import com.matm.matmsdk.aepsmodule.utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

import isumatm.androidsdk.equitas.R;

public class TransactionStatusAeps2Activity extends AppCompatActivity {

    LinearLayout successLayout,failureLayout;
    Button okButton,okSuccessButton;
    TextView detailsTextView,failureDetailTextView,failureTitleTextView;
    String aadharCardStr ="";
    String amount ="";
    Session session;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_statusssss);

        session = new Session(TransactionStatusAeps2Activity.this);
        pd = new ProgressDialog(TransactionStatusAeps2Activity.this);

        successLayout = findViewById(R.id.successLayout);
        failureLayout = findViewById(R.id.failureLayout);
        okButton = findViewById(R.id.okButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        detailsTextView = findViewById(R.id.detailsTextView);
        failureTitleTextView = findViewById(R.id.failureTitleTextView);
        failureDetailTextView = findViewById(R.id.failureDetailTextView);

        if(getIntent().getSerializableExtra(AepsSdkConstants.TRANSACTION_STATUS_KEY) == null){
            failureLayout.setVisibility(View.VISIBLE);
            successLayout.setVisibility(View.GONE);
            failureDetailTextView .setText ( "Some Exception occured");
        }else{
            TransactionStatusModel transactionStatusModel = (TransactionStatusModel) getIntent().getSerializableExtra(AepsSdkConstants.TRANSACTION_STATUS_KEY);

            if (transactionStatusModel.getStatus ().trim ().equalsIgnoreCase ( "0" )) {
                failureLayout.setVisibility ( View.GONE );
                successLayout.setVisibility ( View.VISIBLE );
                String aadharCard = transactionStatusModel.getAadharCard ();
                aadharCardStr = aadharCard;
                amount = transactionStatusModel.getTransactionAmount();
                if (transactionStatusModel.getAadharCard () == null) {
                    aadharCard = "N/A";
                } else {
                    if(transactionStatusModel.getAadharCard ().equalsIgnoreCase ( "" )){
                        aadharCard = "N/A";
                    }else {
                        StringBuffer buf = new StringBuffer( aadharCard );
                        buf.replace ( 0, 10, "XXXX-XXXX-" );
                        System.out.println ( buf.length () );
                        aadharCard = buf.toString ();
                    }
                }

                String bankName = "N/A";
                if (transactionStatusModel.getBankName () != null && !transactionStatusModel.getBankName ().matches ( "" )) {
                    bankName = transactionStatusModel.getBankName ();
                }
                String referenceNo = "N/A";
                if (transactionStatusModel.getReferenceNo () != null && !transactionStatusModel.getReferenceNo ().matches ( "" )) {
                    referenceNo = transactionStatusModel.getReferenceNo ();
                }
                String balance = "N/A";
                if (transactionStatusModel.getBalanceAmount () != null && !transactionStatusModel.getBalanceAmount ().matches ( "" )) {
                    balance = transactionStatusModel.getBalanceAmount ();
                    if (balance.contains ( ":" )) {
                        String[] separated = balance.split ( ":" );
                        balance = separated[ 1 ].trim ();
                    }
                }
                String amount = "N/A";
                if (transactionStatusModel.getTransactionAmount () != null && !transactionStatusModel.getTransactionAmount ().matches ( "" )) {
                    amount = transactionStatusModel.getTransactionAmount ();
                }

                if(transactionStatusModel.getTransactionType ().equalsIgnoreCase ( "Cash Withdrawal" )) {
                    detailsTextView.setText ( transactionStatusModel.getTransactionType () + " for customer account linked with aadhar card " + aadharCard + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + referenceNo + " \n " + "Account Balance : " + balance + "\n Transaction Amount : " + amount );
                }else if(transactionStatusModel.getTransactionType ().equalsIgnoreCase ( "Balance Enquery" )){
                    detailsTextView.setText ( transactionStatusModel.getTransactionType () + " for customer account linked with aadhar card " + aadharCard + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + referenceNo + " \n " + "Account Balance : " + balance );
                }
            }else{
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                failureDetailTextView .setText (transactionStatusModel.getApiComment ());
                failureTitleTextView .setText (transactionStatusModel.getStatusDesc () );
            }
        }
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // finish();
                Intent respIntent = new Intent();
                setResult(Activity.RESULT_OK,respIntent);
                finish();
            }
        });
        okSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // finish();

                if(AepsSdkConstants.transactionType.equalsIgnoreCase(AepsSdkConstants.cashWithdrawal)){
                    ValidateAeps2Transaction(Constants.AADHAR_CARD);

                }else{
                    Intent respIntent = new Intent();
                    setResult(Activity.RESULT_OK,respIntent);
                    finish();
                }
            }
        });

    }

    public void ValidateAeps2Transaction(String cardAadhar) {
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();

        String aadhar_sha = Util.getSha256Hash(cardAadhar);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("retailerId",session.getUserName());
            jsonObject.put("aadhaar_card", aadhar_sha);
            jsonObject.put("amount",amount);

            AndroidNetworking.post(" https://us-central1-creditapp-29bf2.cloudfunctions.net/isuApi/aeps/update_aeps")
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pd.dismiss();
                                JSONObject obj = new JSONObject(response.toString());
                                String statusString = obj.getString("staus");
                                if(statusString.equalsIgnoreCase("1")){
                                    String statusMsg = obj.getString("Message");
                                   // Util.showAlert(TransactionStatusAeps2Activity.this,"Alert",statusMsg);
                                    Toast.makeText(TransactionStatusAeps2Activity.this,statusMsg,Toast.LENGTH_SHORT).show();
                                    Intent respIntent = new Intent();
                                    setResult(Activity.RESULT_OK,respIntent);
                                    finish();

                                }else {
                                    //Transaction Update
                                    Intent respIntent = new Intent();
                                    setResult(Activity.RESULT_OK,respIntent);
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                pd.dismiss();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorBody();
                            pd.dismiss();
                        }
                    });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
