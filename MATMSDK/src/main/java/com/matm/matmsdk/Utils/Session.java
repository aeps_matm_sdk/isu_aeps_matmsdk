package com.matm.matmsdk.Utils;


import android.content.Context;
import android.content.SharedPreferences;


public class Session {
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context ctx;




    public String getUserToken() {
        return prefs.getString(MATMSDKConstant.USER_TOKEN_KEY, null);
    }

    public void setUserToken(String userToken) {
        editor.putString(MATMSDKConstant.USER_TOKEN_KEY, userToken);
        editor.commit();
    }

    public String getFreshnessFactor() {
        return prefs.getString(MATMSDKConstant.NEXT_FRESHNESS_FACTOR, null);
    }

    public void setFreshnessFactor(String freshnessFactor) {
        editor.putString(MATMSDKConstant.NEXT_FRESHNESS_FACTOR, freshnessFactor);
        editor.commit();
    }



    public void clear(){
        prefs = ctx.getSharedPreferences(MATMSDKConstant.EASY_AEPS_PREF_KEY, Context.MODE_PRIVATE);
        editor = prefs.edit();
        editor.clear().commit();
    }


    public Session(Context ctx) {
        this.ctx = ctx;
        prefs = ctx.getSharedPreferences(MATMSDKConstant.EASY_AEPS_PREF_KEY, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public void setLoggedIn(boolean logggedIn) {
        editor.putBoolean(MATMSDKConstant.EASY_AEPS_USER_LOGGED_IN_KEY, logggedIn);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return prefs.getBoolean(MATMSDKConstant.EASY_AEPS_USER_LOGGED_IN_KEY, false);
    }
    public void setUsername(String username) {
        editor.putString (MATMSDKConstant.EASY_AEPS_USER_NAME_KEY, username);
        editor.commit();
    }

    public String getUserName() {
        return prefs.getString (MATMSDKConstant.EASY_AEPS_USER_NAME_KEY, null);
    }
    public void setEncryptedString(String encryptedString) {
        editor.putString (MATMSDKConstant.encryptedString, encryptedString);
        editor.commit();
    }


    public String getEncryptedString() {
        return prefs.getString(MATMSDKConstant.encryptedString, null);
    }
}